warp (0.8.1-4) unstable; urgency=medium

  * d/patches, d/control: Build with asphd 0.11
  * Bump S-V to 4.7.2; no changes needed

 -- Matthias Geiger <werdahias@debian.org>  Mon, 03 Mar 2025 22:23:21 +0100

warp (0.8.1-3) unstable; urgency=medium

  * Upload to unstable

 -- Matthias Geiger <werdahias@debian.org>  Mon, 17 Feb 2025 13:34:30 +0100

warp (0.8.1-2) experimental; urgency=medium

  * Add patch to build with zbus 5.0, ashpd 0.10 and gvdb 0.8
  * d/rules: Disable qr scanning for now

 -- Matthias Geiger <werdahias@debian.org>  Wed, 12 Feb 2025 17:11:29 +0100

warp (0.8.1-1) unstable; urgency=medium

  * New upstream release
  * d/p/rename-warp: Refresh for new upstream release
  * d/p/revert-use-licenses, d/p/remove-win-deps: Likewise

 -- Matthias Geiger <werdahias@debian.org>  Mon, 13 Jan 2025 22:24:40 +0100

warp (0.8.0-2) unstable; urgency=medium

  * Stop patching async-zip (Closes: #1091953)

 -- Matthias Geiger <werdahias@debian.org>  Fri, 03 Jan 2025 15:34:12 +0100

warp (0.8.0-1) unstable; urgency=medium

  * New upstream release
  * Add d/u/metadata
  * Drop d/p/magic-wormhole, d/p/gtk-rs-0.9 and d/p/fix-viewinder; obsoleted by new upstream release
  * Refresh remaining patches for new upstream
  * d/rules: Simplify by including dpkg/default.mk
  * d/rules: Disable lto on 32-bit arches
  * Cherry-pick upstream commit to revert introduction of license compatibilty crate
  * d/control: Updated dependencies for new upstream release
  * Add patch to downgrade async-zip to 0.0.16

 -- Matthias Geiger <werdahias@debian.org>  Sat, 26 Oct 2024 18:14:43 +0200

warp (0.7.0-2) unstable; urgency=medium

  * Add patch to rename binary to warp-gtk (Closes: #1084821)
  * Cherry-pick upstream patch to enable qr-code scanning,
    depend on packages needed to do so, set buildflag in d/rules
  * Upload to unstable

 -- Matthias Geiger <werdahias@debian.org>  Mon, 21 Oct 2024 16:32:50 +0200

warp (0.7.0-1) experimental; urgency=medium

  * Initial release. (Closes: #1040664)

 -- Matthias Geiger <werdahias@debian.org>  Mon, 30 Sep 2024 19:48:54 +0200
