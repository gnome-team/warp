# Chinese translation for warp.
# Copyright (C) YEAR THE warp'S COPYRIGHT HOLDER
# This file is distributed under the same license as the warp package.
# Sirniu <sirniu@qq.com>, 2022.
# lumingzh <lumingzh@qq.com>, 2022-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: warp main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/warp/issues\n"
"POT-Creation-Date: 2024-11-24 02:55+0000\n"
"PO-Revision-Date: 2024-11-27 08:32+0800\n"
"Last-Translator: lumingzh <lumingzh@qq.com>\n"
"Language-Team: Chinese (China) <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 47.0\n"

#: data/app.drey.Warp.desktop.in.in:3 data/app.drey.Warp.metainfo.xml.in.in:4
#: src/main.rs:131 src/ui/window.ui:81
msgid "Warp"
msgstr "Warp"

#: data/app.drey.Warp.desktop.in.in:4 data/app.drey.Warp.metainfo.xml.in.in:5
#: src/ui/welcome_dialog.ui:24
msgid "Fast and secure file transfer"
msgstr "快捷、安全的文件传输工具"

#: data/app.drey.Warp.desktop.in.in:9
msgid "Gnome;GTK;Wormhole;Magic-Wormhole;"
msgstr "Gnome;GTK;Wormhole;Magic-Wormhole;虫洞;魔法虫洞;传输;传送;"

#. developer_name tag deprecated with Appstream 1.0
#: data/app.drey.Warp.metainfo.xml.in.in:9 src/ui/window.rs:303
msgid "Fina Wilke"
msgstr "Fina Wilke"

#: data/app.drey.Warp.metainfo.xml.in.in:43
msgid ""
"Warp allows you to securely send files to each other via the internet or "
"local network by exchanging a word-based code."
msgstr "使用 Warp 可以基于文本代码在互联网或局域网中安全地进行文件交换。"

#: data/app.drey.Warp.metainfo.xml.in.in:47
msgid ""
"The best transfer method will be determined using the “Magic Wormhole” "
"protocol which includes local network transfer if possible."
msgstr ""
"好的文件传输方式应当致力于尽可能使用具有本地网络传输功能的“魔法虫洞”协议。"

#: data/app.drey.Warp.metainfo.xml.in.in:51
msgid "Features"
msgstr "特性"

#: data/app.drey.Warp.metainfo.xml.in.in:53
msgid "Send files between multiple devices"
msgstr "多设备间发送文件"

#: data/app.drey.Warp.metainfo.xml.in.in:54
msgid "Every file transfer is encrypted"
msgstr "所传输的每个文件都进行了加密"

#: data/app.drey.Warp.metainfo.xml.in.in:55
msgid "Directly transfer files on the local network if possible"
msgstr "如果可能的话在局域网中使用直传方式"

#: data/app.drey.Warp.metainfo.xml.in.in:56
msgid "An internet connection is required"
msgstr "请先连接网络"

#: data/app.drey.Warp.metainfo.xml.in.in:57
msgid "QR Code support"
msgstr "支持二维码"

#: data/app.drey.Warp.metainfo.xml.in.in:58
msgid ""
"Compatibility with the Magic Wormhole command line client and all other "
"compatible apps"
msgstr "与“魔法虫洞”命令行客户端以及其它同类应用保持兼容"

#: data/app.drey.Warp.metainfo.xml.in.in:64
#: data/app.drey.Warp.metainfo.xml.in.in:84
msgid "Main Window"
msgstr "主窗口"

#. Translators: Entry placeholder; This is a noun
#: data/app.drey.Warp.metainfo.xml.in.in:68
#: data/app.drey.Warp.metainfo.xml.in.in:88 src/ui/window.ui:207
msgid "Transmit Code"
msgstr "传输代码"

#: data/app.drey.Warp.metainfo.xml.in.in:72
#: data/app.drey.Warp.metainfo.xml.in.in:92
msgid "Accept File Transfer"
msgstr "接受文件传输请求"

#: data/app.drey.Warp.metainfo.xml.in.in:76
#: data/app.drey.Warp.metainfo.xml.in.in:96 src/ui/action_view.rs:646
msgid "Receiving File"
msgstr "文件接收中"

#: data/app.drey.Warp.metainfo.xml.in.in:80
#: data/app.drey.Warp.metainfo.xml.in.in:100 src/ui/action_view.rs:655
msgid "File Transfer Complete"
msgstr "文件传输完成"

#: data/resources/ui/help_overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "常规"

#: data/resources/ui/help_overlay.ui:14
msgctxt "shortcut window"
msgid "Show Help"
msgstr "显示帮助"

#: data/resources/ui/help_overlay.ui:20
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "显示快捷键"

#: data/resources/ui/help_overlay.ui:26
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "显示首选项"

#: data/resources/ui/help_overlay.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "退出"

#: data/resources/ui/help_overlay.ui:40
msgctxt "shortcut window"
msgid "File Transfer"
msgstr "文件传输器"

#: data/resources/ui/help_overlay.ui:43
msgctxt "shortcut window"
msgid "Send File"
msgstr "发送文件"

#: data/resources/ui/help_overlay.ui:49
msgctxt "shortcut window"
msgid "Send Folder"
msgstr "发送目录"

#: data/resources/ui/help_overlay.ui:55
msgctxt "shortcut window"
msgid "Receive File"
msgstr "接收文件"

#. Translators: {0} = file size transferred, {1} = total file size, Example: 17.3MB / 20.5MB
#: src/gettext/duration.rs:11
msgctxt "File size transferred"
msgid "{0} / {1}"
msgstr "{0} / {1}"

#. Translators: File transfer time left
#: src/gettext/duration.rs:18
msgid "One second left"
msgid_plural "{} seconds left"
msgstr[0] "剩余 {} 秒"

#. Translators: File transfer time left
#: src/gettext/duration.rs:26
msgid "One minute left"
msgid_plural "{} minutes left"
msgstr[0] "剩余 {} 分钟"

#. Translators: File transfer time left
#: src/gettext/duration.rs:34
msgid "One hour left"
msgid_plural "{} hours left"
msgstr[0] "剩余 {} 小时"

#. Translators: File transfer time left
#: src/gettext/duration.rs:42
msgid "One day left"
msgid_plural "{} days left"
msgstr[0] "剩余 {} 天"

#. Translators: {0} = 11.3MB / 20.7MB, {1} = 3 seconds left
#: src/gettext/duration.rs:52
msgctxt "Combine bytes progress {0} and time remaining {1}"
msgid "{0} — {1}"
msgstr "{0} - {1}"

#. Translators: Notification when clicking on "Copy Code to Clipboard" button
#: src/ui/action_view.rs:256
msgid "Copied Code to Clipboard"
msgstr "代码已复制到剪切板"

#. Translators: Notification when clicking on "Copy Link to Clipboard" button
#: src/ui/action_view.rs:273
msgid "Copied Link to Clipboard"
msgstr "链接已复制到剪切板"

#: src/ui/action_view.rs:287
msgid "Copied Error to Clipboard"
msgstr "错误信息已复制到剪切板"

#: src/ui/action_view.rs:289
msgid "No error available"
msgstr "没有错误"

#: src/ui/action_view.rs:454
msgid "Creating Archive"
msgstr "正在创建归档"

#: src/ui/action_view.rs:458
msgid "Compressing folder “{}”"
msgstr "正在压缩文件夹“{}”"

#. Translators: Title
#: src/ui/action_view.rs:474 src/ui/action_view.rs:537
msgid "Connecting"
msgstr "正在连接"

#. Translators: Description, Filename
#: src/ui/action_view.rs:477
msgid "Requesting file transfer"
msgstr "正在请求文件传输"

#. Translators: Description, argument is filename
#: src/ui/action_view.rs:498
msgid "Ready to send “{}”."
msgstr "已准备好发送 “{}”。"

#. Translators: Help dialog line 1, Code words and QR code visible,
#: src/ui/action_view.rs:503
msgid ""
"The receiver needs to enter or scan this code to begin the file transfer."
msgstr "接收者需要输入该代码或扫描二维码来开始文件传输。"

#: src/ui/action_view.rs:507
msgid "The QR code is compatible with the following apps: {}."
msgstr "二维码兼容下列应用：{}。"

#: src/ui/action_view.rs:515
msgid ""
"You have entered a custom rendezvous server URL in preferences. Please "
"verify the receiver also uses the same rendezvous server."
msgstr ""
"您已在首选项中输入了自定义的集合服务器网址。请核实接收者同样使用相同的集合服"
"务器。"

#: src/ui/action_view.rs:520
msgid "Click the QR code to copy the link to the clipboard."
msgstr "点击二维码将链接复制到剪贴板。"

#. Translators: Description, Transfer Code
#: src/ui/action_view.rs:540
msgid "Connecting to peer with code “{}”"
msgstr "正在使用代码“{}”连接至对等设备"

#: src/ui/action_view.rs:550
msgid "Connected to Peer"
msgstr "已连接至对等设备"

#. Translators: Description
#: src/ui/action_view.rs:559
msgid "Preparing to send file"
msgstr "正在准备发送文件"

#. Translators: Description
#: src/ui/action_view.rs:566
msgid "Preparing to receive file"
msgstr "正在准备接收文件"

#. Translators: File receive confirmation message dialog; Filename, File size
#: src/ui/action_view.rs:580
msgid ""
"Your peer wants to send you “{0}” (Size: {1}).\n"
"Do you want to download this file? The default action will save the file to "
"your Downloads folder."
msgstr ""
"您的对等设备想要给您发送“{0}”（大小：{1}）。\n"
"您想要下载该文件吗？默认动作会将文件保存到下载文件夹。"

#: src/ui/action_view.rs:585
msgid "Ready to Receive File"
msgstr "已准备好接收文件"

#: src/ui/action_view.rs:587
msgid ""
"A file is ready to be transferred. The transfer needs to be acknowledged."
msgstr "文件已准备好传输，传输者需被告知。"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:617
msgid "File “{}” via local network direct transfer"
msgstr "文件“{}”通过本地网络使用直传方式"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:620
msgid "File “{}” via direct transfer"
msgstr "文件“{}”使用直传方式"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:626
msgid "File “{0}” via relay {1}"
msgstr "文件“{0}”通过 {1} 中转"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:629
msgid "File “{}” via relay"
msgstr "文件“{}”通过中转传输"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:633
msgid "File “{}” via Unknown connection method"
msgstr "文件“{}”通过不明连接方式传输"

#. Translators: Title
#: src/ui/action_view.rs:640
msgid "Sending File"
msgstr "文件发送中"

#. Translators: Description, Filename
#: src/ui/action_view.rs:663
msgid "Successfully sent file “{}”"
msgstr "文件“{}”发送成功"

#. Translators: Filename
#: src/ui/action_view.rs:681
msgid "File has been saved to the selected folder as “{}”"
msgstr "文件已作为“{}”保存到指定文件夹"

#. Translators: Filename
#: src/ui/action_view.rs:688
msgid "File has been saved to the Downloads folder as “{}”"
msgstr "文件已作为“{}”保存到下载文件夹"

#. Translators: Title
#: src/ui/action_view.rs:714 src/ui/action_view.ui:292
msgid "File Transfer Failed"
msgstr "文件传输失败"

#: src/ui/action_view.rs:716
msgid "The file transfer failed: {}"
msgstr "文件传输失败：{}"

#. Translators: When opening a file
#: src/ui/action_view.rs:834
msgid "Specified file / directory does not exist"
msgstr "指定文件/目录不存在"

#: src/ui/action_view.rs:856
msgid ""
"Error parsing rendezvous server URL. An invalid URL was entered in the "
"settings."
msgstr "集合服务器网址解析错误。设置中输入了无效的网址。"

#: src/ui/action_view.rs:863
msgid "Error parsing transit URL. An invalid URL was entered in the settings."
msgstr "传输网址解析错误。设置中输入了无效的网址。"

#: src/ui/action_view.rs:959
msgid "Invalid path selected: {}"
msgstr "已选路径无效：{}"

#. Translators: Above progress bar for creating an archive to send as a folder
#: src/ui/action_view.rs:1204
msgid "{} File - Size: {}"
msgid_plural "{} Files - Size: {}"
msgstr[0] "{} 个文件 - 文件大小：{}"

#: src/ui/action_view.ui:4
msgid "Save As"
msgstr "另存为"

#. Translators: Button; Transmit Link is a noun
#: src/ui/action_view.ui:108
msgid "Copy Transmit Link"
msgstr "复制传输链接"

#: src/ui/action_view.ui:133
msgid "Your Transmit Code"
msgstr "您的传输代码"

#. Translators: Button; Transmit Code is a noun
#: src/ui/action_view.ui:181
msgid "Copy Transmit Code"
msgstr "复制传输代码"

#. Translators: Title
#: src/ui/action_view.ui:198
msgid "Accept File Transfer?"
msgstr "接受文件传输吗？"

#: src/ui/action_view.ui:208
msgid "Save to Downloads Folder"
msgstr "保存至下载文件夹"

#. Translators: Button
#: src/ui/action_view.ui:210
msgid "_Accept"
msgstr "接受(_A)"

#. Translators: Button
#: src/ui/action_view.ui:225
msgid "Sa_ve As…"
msgstr "另存为(_V)…"

#. Translators: Title
#: src/ui/action_view.ui:245
msgid "File Transfer Successful"
msgstr "文件传输成功"

#. Translators: Button
#: src/ui/action_view.ui:255
msgid "_Open File"
msgstr "打开文件(_O)"

#. Translators: Button
#: src/ui/action_view.ui:271 src/ui/window.ui:51
msgid "_Show in Folder"
msgstr "在文件夹中显示(_S)"

#. Translators: Button
#: src/ui/action_view.ui:298
msgid "Co_py Error Message"
msgstr "复制错误信息(_P)"

#. Translators: Button
#: src/ui/action_view.ui:316
msgid "_Cancel"
msgstr "取消(_C)"

#: src/ui/application.rs:60
msgid "Unable to use transfer link: another transfer already in progress"
msgstr "无法使用传输链接：当前有传输正在进行中"

#: src/ui/application.rs:207
msgid "Sending a File"
msgstr "正在发送文件"

#: src/ui/application.rs:208
msgid "Receiving a File"
msgstr "正在接收文件"

#: src/ui/camera.rs:245
msgid ""
"Camera access denied. Open Settings and allow Warp to access the camera."
msgstr "摄像头访问被拒绝。请打开设置并允许 Warp 访问摄像头。"

#: src/ui/camera.rs:249
msgid "Failed to start the camera: {}"
msgstr "启动摄像头失败：{}"

#: src/ui/camera.rs:252
msgid "Error"
msgstr "错误"

#: src/ui/camera.rs:341
msgid "Could not use the camera portal: {}"
msgstr "无法使用摄像头门户：{}"

#: src/ui/camera.rs:348
msgid "Could not start the device provider: {}"
msgstr "无法启动该设备提供者：{}"

#: src/ui/camera.rs:379
msgid "No Camera Found"
msgstr "未找到摄像头"

#: src/ui/camera.rs:381
msgid "Connect a camera to scan QR codes"
msgstr "连接摄像头来扫描二维码"

#: src/ui/camera.ui:11 src/ui/window.ui:216
msgid "Scan QR Code"
msgstr "扫描二维码"

#: src/ui/camera.ui:48
msgid "_Retry"
msgstr "重试(_R)"

#: src/ui/camera.ui:59
msgid "_Troubleshooting"
msgstr "故障排除(_T)"

#: src/ui/fs.rs:16
msgid "Downloads dir missing. Please set XDG_DOWNLOAD_DIR"
msgstr "下载目录缺失。请设置 XDG_DOWNLOAD_DIR"

#: src/ui/licenses.rs:149
msgid "Licensed under the <a href=\"{}\">{}</a>."
msgstr "使用 <a href=\"{}\">{}</a> 许可证。"

#. Translators: License information without a link to the software license
#: src/ui/licenses.rs:154
msgid "Licensed under the {}."
msgstr "使用 {} 许可证。"

#: src/ui/preferences.rs:97
msgid ""
"Changing the rendezvous server URL needs to be done on both sides of the "
"transfer. Only enter a server URL you can trust.\n"
"\n"
"Leaving these entries empty will use the app defaults:\n"
"Rendezvous Server: “{0}”\n"
"Transit Server: “{1}”"
msgstr ""
"该传输需要修改传输工具两端的集合服务器网址。请输入可信任的服务器网址。\n"
"\n"
"这些条目留空将使用应用程序的默认值：\n"
"集合服务器：“{0}”\n"
"传输服务器：“{1}”"

#: src/ui/preferences.ui:4
msgid "Preferences"
msgstr "首选项"

#: src/ui/preferences.ui:8
msgid "Network"
msgstr "网络"

#: src/ui/preferences.ui:12
msgid "Code Words"
msgstr "代码语句"

#: src/ui/preferences.ui:13
msgid ""
"The code word count determines the security of the transfer.\n"
"\n"
"A short code is easy to remember but increases the risk of someone else "
"guessing the code. As a code may only be guessed once, the risk is very "
"small even with short codes. A length of 4 is very secure."
msgstr ""
"代码单词总数决定了传输的安全性。\n"
"\n"
"简短的代码容易记忆但增加了他人猜测代码的风险。由于一个代码可能仅会被猜测一"
"次，即使使用简短的代码风险也是很小。4 个单词的长度会非常安全。"

#: src/ui/preferences.ui:18
msgid "Code Word Count"
msgstr "代码单词总数"

#: src/ui/preferences.ui:26
msgid "Server URLs"
msgstr "服务器网址"

#: src/ui/preferences.ui:29
msgid "Rendezvous Server URL"
msgstr "集合服务器网址"

#: src/ui/preferences.ui:36
msgid "Transit Server URL"
msgstr "传输服务器网址"

#: src/ui/welcome_dialog.ui:10
msgid "Welcome"
msgstr "欢迎"

#: src/ui/welcome_dialog.ui:23
msgid "Welcome to Warp"
msgstr "欢迎使用 Warp"

#. Translators: Button
#: src/ui/welcome_dialog.ui:31
msgid "Next"
msgstr "下一步"

#: src/ui/welcome_dialog.ui:79
msgid "Introduction"
msgstr "介绍"

#: src/ui/welcome_dialog.ui:90
msgid ""
"Warp makes file transfer simple. To get started, both parties need to "
"install Warp on their devices.\n"
"\n"
"After selecting a file to transmit, the sender needs to tell the receiver "
"the displayed transmit code. This is preferably done via a secure "
"communication channel.\n"
"\n"
"When the receiver has entered the code, the file transfer can begin.\n"
"\n"
"For more information about Warp, open the Help section from the Main Menu."
msgstr ""
"Warp 让文件传输变得简单，想要开始使用的话，团队成员都需将 Warp 安装到他们的设"
"备上。\n"
"\n"
"选择文件并开启传输后，文件发送者需将显示的传输码告知接收者。这一步建议通过安"
"全的沟通渠道来完成。\n"
"\n"
"接收者输入传输码后文件开始传输。\n"
"\n"
"如果想获取关于 Warp 的更多信息，可从主菜单中打开帮助页查看。"

#. Translators: Big button to finish welcome screen
#: src/ui/welcome_dialog.ui:103
msgid "Get Started Using Warp"
msgstr "开始使用 Warp"

#: src/ui/window.rs:135
msgid ""
"Error loading config file “{0}”, using default config.\n"
"Error: {1}"
msgstr ""
"加载配置文件“{0}”出错，使用默认配置。\n"
"错误：{1}"

#: src/ui/window.rs:284
msgid "Error saving configuration file: {}"
msgstr "保存配置文件时出错：{}"

#: src/ui/window.rs:304
msgid "Tobias Bernard"
msgstr "Tobias Bernard"

#: src/ui/window.rs:304
msgid "Sophie Herold"
msgstr "Sophie Herold"

#: src/ui/window.rs:305
msgid "translator-credits"
msgstr ""
"Sirniu <sirniu@qq.com>, 2022.\n"
"lumingzh <lumingzh@qq.com>, 2022-2024.\n"
"Eni <enigma@petalmail.com>, 2023."

#: src/ui/window.rs:370
msgid "Select File to Send"
msgstr "选择要发送的文件"

#: src/ui/window.rs:378
msgid "Select Folder to Send"
msgstr "选择要发送的文件夹"

#: src/ui/window.rs:441
msgid "“{}” appears to be an invalid Transmit Code. Please try again."
msgstr "“{}”可能是无效的传输代码。请重试。"

#: src/ui/window.rs:565
msgid "Sending files with a preconfigured code is not yet supported"
msgstr "尚未支持使用预配置代码发送文件"

#. Translators: menu item
#: src/ui/window.ui:7
msgid "_Preferences"
msgstr "首选项(_P)"

#. Translators: menu item
#: src/ui/window.ui:12
msgid "_Keyboard Shortcuts"
msgstr "键盘快捷键(_K)"

#. Translators: menu item
#: src/ui/window.ui:17
msgid "_Help"
msgstr "帮助(_H)"

#. Translators: menu item
#: src/ui/window.ui:22
msgid "_About Warp"
msgstr "关于 Warp(_A)"

#. Translators: Notification when code was automatically detected in clipboard and inserted into code entry on receive page
#: src/ui/window.ui:33
msgid "Inserted code from clipboard"
msgstr "从剪贴板插入代码"

#. Translators: File receive confirmation message dialog title
#: src/ui/window.ui:38
msgid "Abort File Transfer?"
msgstr "中止文件传输吗？"

#: src/ui/window.ui:39
msgid "Do you want to abort the current file transfer?"
msgstr "您想要中止当前文件传输吗？"

#: src/ui/window.ui:42
msgid "_Continue"
msgstr "继续(_C)"

#: src/ui/window.ui:43
msgid "_Abort"
msgstr "中止(_A)"

#. Translators: Error dialog title
#: src/ui/window.ui:48
msgid "Unable to Open File"
msgstr "无法打开文件"

#: src/ui/window.ui:50 src/util/error.rs:207
msgid "_Close"
msgstr "关闭(_C)"

#: src/ui/window.ui:98
msgid "Main Menu"
msgstr "主菜单"

#: src/ui/window.ui:117
msgid "_Send"
msgstr "发送(_S)"

#: src/ui/window.ui:129
msgid "Send File"
msgstr "发送文件"

#: src/ui/window.ui:130
msgid "Select or drop the file or directory to send"
msgstr "选择或拖拽文件、目录进行发送"

#. Translators: Button
#: src/ui/window.ui:142
msgid "Select _File…"
msgstr "选择文件(_S)"

#. Translators: Button
#: src/ui/window.ui:157
msgid "Select F_older…"
msgstr "选择文件夹(_O)"

#: src/ui/window.ui:177
msgid "_Receive"
msgstr "接收(_R)"

#: src/ui/window.ui:186
msgid "Receive File"
msgstr "接收文件"

#. Translators: Text above code input box, transmit code is a noun
#: src/ui/window.ui:188
msgid "Enter the transmit code from the sender"
msgstr "输入发送者提供的传输码"

#. Translators: Button
#: src/ui/window.ui:237
msgid "Receive _File"
msgstr "接收文件(_F)"

#: src/ui/window.ui:263
msgid "File Transfer"
msgstr "文件传输器"

#: src/util.rs:37 src/util.rs:43
msgid "Failed to open downloads folder."
msgstr "打开下载文件夹失败。"

#: src/util.rs:216 src/util.rs:291
msgid "The URI format is invalid"
msgstr "网址格式无效"

#: src/util.rs:220 src/util.rs:224
msgid "The code does not match the required format"
msgstr "代码与需要的格式不匹配"

#: src/util.rs:241 src/util.rs:247
msgid "Unknown URI version: {}"
msgstr "未知网址版本：{}"

#: src/util.rs:256
msgid "The URI parameter “rendezvous” contains an invalid URL: “{}”"
msgstr "网址参数“rendezvous”包含无效网址：“{}”"

#: src/util.rs:268
msgid "The URI parameter “role” must be “follower” or “leader” (was: “{}”)"
msgstr "网址参数“role”必须为“follower”或“leader”（曾为：“{}”）"

#: src/util.rs:275
msgid "Unknown URI parameter “{}”"
msgstr "未知网址参数“{}”"

#: src/util/error.rs:190
msgid "An error occurred"
msgstr "发生错误"

#: src/util/error.rs:218 src/util/error.rs:333
msgid "Corrupt or unexpected message received"
msgstr "接收到损坏或意外消息"

#: src/util/error.rs:223
msgid ""
"The rendezvous server will not allow further connections for this code. A "
"new code needs to be generated."
msgstr "该集合服务器不允许该代码的进一步连接。需要生成新代码。"

#: src/util/error.rs:225
msgid ""
"The rendezvous server removed the code due to inactivity. A new code needs "
"to be generated."
msgstr "该集合服务器已因不活跃移除了该代码。需要生成新代码。"

#: src/util/error.rs:227
msgid "The rendezvous server responded with an unknown message: {}"
msgstr "该集合服务器使用了未知消息响应：{}"

#: src/util/error.rs:230
msgid ""
"Error connecting to the rendezvous server.\n"
"You have entered a custom rendezvous server URL in preferences. Please "
"verify the URL is correct and the server is working."
msgstr ""
"连接至集合服务器时出错。\n"
"您在首选项中输入了自定义集合服务器网址。请核实该网址是否正确且服务器正常工"
"作。"

#: src/util/error.rs:232
msgid ""
"Error connecting to the rendezvous server.\n"
"Please try again later / verify you are connected to the internet."
msgstr ""
"连接至集合服务时出错。\n"
"请稍后重试/验证您已连接至互联网。"

#: src/util/error.rs:236
msgid ""
"Encryption key confirmation failed. If you or your peer didn't mistype the "
"code, this is a sign of an attacker guessing passwords. Please try again "
"some time later."
msgstr ""
"加密秘钥确认失败。如果您和您的对等设备都输入了正确的代码，有可能您正在遭受密"
"码猜测攻击。请稍后重试。"

#: src/util/error.rs:238
msgid "Cannot decrypt a received message"
msgstr "无法解密已接收消息"

#: src/util/error.rs:239 src/util/error.rs:344 src/util/error.rs:351
msgid "An unknown error occurred"
msgstr "发生未知错误"

#. Translators: magic-wormhole codes usually look like n-word-word... where n MUST be an arabic number (unlimited digits). The first '-' is mandatory. This error occurs when a code was entered that does not contain any '-'.
#: src/util/error.rs:246
msgid "The specified code must not be empty"
msgstr "指定代码不能为空"

#. Translators: magic-wormhole codes usually look like n-word-word... where n MUST be an arabic number (unlimited digits). The first '-' is mandatory. This error occurs when a code was entered that does not contain any '-'.
#: src/util/error.rs:248
msgid "The code must contain at least one “-”"
msgstr "代码必须包含至少一个“-”"

#. Translators: magic-wormhole codes usually look like n-word-word... where n MUST be an arabic number (unlimited digits). This error occurs when a code was entered that does not contain a number before the '-'.
#: src/util/error.rs:250
msgid "The first segment of a code must be a number"
msgstr "代码的首个片段必须为一个数字"

#: src/util/error.rs:254
msgid "Code too weak. It can be guessed with an average of {} try."
msgid_plural "Code too weak. It can be guessed with an average of {} tries."
msgstr[0] "代码太简单。平均 {} 次尝试即可猜到。"

#: src/util/error.rs:262
msgid ""
"Code too short. The text after the “-” must be at least {} character long."
msgid_plural ""
"Code too short. The text after the “-” must be at least {} characters long."
msgstr[0] "代码太短了。在“-”后面的文本必须有至少 {} 个字符的长度。"

#: src/util/error.rs:269
msgid "Code too weak"
msgstr "代码太简单"

#: src/util/error.rs:276
msgid "File / Directory not found"
msgstr "未找到文件/目录"

#: src/util/error.rs:277
msgid "Permission denied"
msgstr "无权访问"

#: src/util/error.rs:285
msgid "Canceled"
msgstr "已取消"

#: src/util/error.rs:287
msgid "Portal service response: Permission denied"
msgstr "门户服务响应：权限被拒绝"

#: src/util/error.rs:290
msgid "The portal service has failed: {}"
msgstr "门户服务失败：{}"

#: src/util/error.rs:293
msgid "Error communicating with the portal service via zbus: {}"
msgstr "通过 zbus 沟通门户服务出错：{}"

#: src/util/error.rs:296 src/util/error.rs:297
msgid "Portal error: {}"
msgstr "门户错误：{}"

#: src/util/error.rs:308
msgid "Transfer was not acknowledged by peer"
msgstr "对等设备未被告知传输"

#: src/util/error.rs:310
msgid "The received file is corrupted"
msgstr "接收的文件已损坏"

#: src/util/error.rs:316
msgid ""
"The file contained a different amount of bytes than advertised! Sent {} "
"bytes, but should have been {}"
msgstr "文件包含的字节数量与告知的不一致！发送了 {} 字节，应当有 {} 字节。"

#: src/util/error.rs:321
msgid "The other side has cancelled the transfer"
msgstr "对方取消了传输"

#: src/util/error.rs:323
msgid "The other side has rejected the transfer"
msgstr "对方拒绝了传输"

#: src/util/error.rs:325
msgid "Something went wrong on the other side: {}"
msgstr "对方出错：{}"

#: src/util/error.rs:340
msgid "Error while establishing file transfer connection"
msgstr "建立文件传输连接时出错"

#: src/util/error.rs:342
msgid "Unknown file transfer error"
msgstr "未知的文件传输错误"

#: src/util/error.rs:352
msgid "An unknown error occurred while creating a zip file: {}"
msgstr "在创建 zip 文件时发生未知错误：{}"

#: src/util/error.rs:353
msgid ""
"An unexpected error occurred. Please report an issue with the error message."
msgstr "发生了意外错误。请提交带有该错误信息的报告。"

#~ msgid "Error talking to the camera portal"
#~ msgstr "沟通摄像头门户出错"

#~ msgid "Portal Error: Failed: {}"
#~ msgstr "门户错误：已失败：{}"

#~ msgid "Portal Error: {}"
#~ msgstr "门户错误：{}"

#~ msgid "Accept File Transfer and save to Downloads folder"
#~ msgstr "接受文件传输并保存到下载文件夹"

#~ msgid "Select Save Location"
#~ msgstr "选择保存位置"

#~ msgid "Show in Folder"
#~ msgstr "在文件夹中显示"

#~ msgid "Copy a detailed message for reporting an issue"
#~ msgstr "复制详细信息用于上报问题"

#~ msgid "Save"
#~ msgstr "保存"

#~ msgid "Open"
#~ msgstr "打开"

#~ msgid "Open Folder"
#~ msgstr "打开文件夹"

#~ msgid "Back"
#~ msgstr "返回"

#~ msgid "Close"
#~ msgstr "关闭"

#~ msgid "Select the file or directory to send"
#~ msgstr "选择文件或目录进行发送"

#~ msgid "Path {} does not have a directory name"
#~ msgstr "路径 {} 没有目录名称"

#~ msgid "Added French translation"
#~ msgstr "增加法语翻译"

#~ msgid "Metadata updates and bugfixes"
#~ msgstr "元数据更新和bug修复"

#~ msgid "Initial release"
#~ msgstr "首发版本"

#~ msgid "_Open Folder"
#~ msgstr "打开目录(O)"

#~ msgid "Your Transfer Code"
#~ msgstr "传输码"

#~ msgid "Unable to parse transfer link. The link needs to be in the format {}"
#~ msgstr "无法解析传输链接。链接的格式应当是这样 {}"
